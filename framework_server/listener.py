#!/usr/bin/python
# -*- coding: utf-8 -*-

# File: listener.py
# listener Object

import threading, time
import rethinkdb as r

# Class: Listener
# 
# Holds a thread that analyses a change feed installed on one or more Player Status Databases.
# If conditions match the change its closed.
class Listener(object):

	def __init__(self, properties, cuename, cue_bucket, db_ip, db_port):
		self.origin_cue = cuename
		self.followcue = properties['Followcue']

		# Type defaults to infinite
		if 'Type' in properties:
			self.type = properties['Type']
		else:
			self.type = 'infinite'

		self.properties = properties
		self.cue_bucket = cue_bucket

		self.player = r.db("status").table("player")

		# If Listener comes without name, it can be identified through the cue its called by
		if 'Name' in properties:
			self.name = properties['Name']
		else:
			self.name = self.origin_cue

		if "Followcue" in properties:
			self.conn = r.connect(db_ip, db_port)
			self.feed = ""

			if "Target" in properties:
				self.target = properties.pop("Target")
				feed_query_str = 'self.feed = r.db("status").table("player").filter(%s).changes(include_initial=True).run(self.conn)' % self.target
			elif "Query" in properties:
				self.query = properties.pop("Query")
				feed_query_str = 'self.feed = %s.run(self.conn)' % self.query
			else:
				print("ERROR: Could not find 'Query' or 'Target' Property in Listener %s" % self.name)
				self.open = False
				return


			print ("change feed '%s' query: %s. Condition: %s. Followcue: %s" % (self.name, feed_query_str, self.properties["Condition"], self.properties['Followcue']))

			feed_query = compile(feed_query_str, 'feed_query', 'exec')

			try:
				self.open = True
				
				exec(feed_query)

				self.listen_thread = threading.Thread(name = '%s_listener_thread' % self.name, target = self.change_feed, args = ())
				self.listen_thread.start()

				#print ("Change Feed return value: %s" % self.feed)
			except (SyntaxError, NameError) as e:
				print "ERROR: %s --- IN Listener %s %s" % (e, self.name, self.properties)
				self.open = False

		else:
			print( "ERROR: No Followcue defined to install Listener change feed %s" % self.name )
			self.open = False

		print("\n")

	# Function: change_feed
	# waits for changes in specified Player Objects.
	def change_feed(self):
		# execute condition line and put result into match variable
		match = False
		condition_query = 'match = %s' % self.properties["Condition"]
		query = compile(condition_query, 'query', 'exec')
		
		for change in self.feed:
			# print change['new_val']

			if change is not None:
				if 'name' in change['new_val']:
					pass
					# print("Review '%s' Condition: %s" % (change['new_val']['name'], condition_query) )
				else:
					# print("Review %s Condition: %s" % ("'unknown'", condition_query) )
					pass

				try:
					
					exec(query)

					if match:
						print("Match in Listener %s. Followcue: %s" % (self.name, self.properties['Followcue']))
						followcue = {"Name":self.properties["Followcue"]}
						#self.cue_bucket.append(self.properties['Followcue'])
						self.cue_bucket.append(followcue)

						# type
						# 'close_on_match' - close listener and thread on first match
						# 'infinite' -  keep listener up. Close has to be done manually
						# Default is to maintain listener
						if self.type == "close_on_match":
							self.open = False
							#self.close()
							break
					else:
						#print("No match in Listener %s" % self.name)
						pass # never mind


				except SyntaxError as e:
					print "Syntax Error in Listener %s: %s" % (self.name, e)
				except KeyError as e:
					print "Key Error in Listener %s: %s" % (self.name, e)

			else:
				print("Missing change values in " + self.name)

		print ("Ended: %s" % self.listen_thread.name)
		self.conn.close()

	time.sleep(1)


	def close(self):
		self.open = False

		if self.listen_thread.isAlive():
			self.feed.close()
			self.listen_thread.join(2.0)
			if self.listen_thread.isAlive():
				print('could not join thread: %s' % self.listen_thread.name)

		self.conn.close()

		
		

	# Obsolte. Probably useful for other exec places. Could go in some toolset module
	def exec_query(self, exec_str):
		query = compile('return_val = ' + exec_str, 'query', 'exec')

		try:
			exec(query)
			return return_val
		except SyntaxError as e:
			print e