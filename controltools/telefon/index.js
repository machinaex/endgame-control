var app = require('express')();
var http = require('http').Server(app);
var howler = require("howler");
var io = require('socket.io')(http);
var net = require('net');

var sendHOST = '192.168.0.2'; // IP to forward status msg to
var sendPORT = 9000; // Port to forward to
var inputPort = 9001;


app.get('/ring.mp3', function(req, res){
  res.sendFile(__dirname + '/ring.mp3');
});

app.get('/sound.mp3', function(req, res){
  res.sendFile(__dirname + '/sound.mp3');
});

app.get('/mumblecore.mp3', function(req, res){
  res.sendFile(__dirname + '/mumblecore.mp3');
});

app.get('/polizei.mp3', function(req, res){
  res.sendFile(__dirname + '/polizei.mp3');
});

app.get('/troll.mp3', function(req, res){
  res.sendFile(__dirname + '/troll.mp3');
});

app.get('/howler.js', function(req, res){
  res.sendFile(__dirname + '/howler.js');
});

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
  console.log(__dirname);
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
  	console.log("WOOOOOW " + msg);
    io.emit('chat message', msg);
    try{sendToMax(msg)}catch(e){console.log("ERROR: could not forward to Max")}
  });
});


http.listen(3000, function(){
  console.log('listening on *:3000');
});

////////////////////////////////////////////////////

console.log("now starting TCP server...");

var server = net.createServer();  
server.on('connection', handleConnection);

server.listen(inputPort,"0.0.0.0", function() {  
  console.log('server listening to %j', server.address());
});

function handleConnection(conn) {  
  var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
  console.log('new client connection from %s', remoteAddress);

  conn.setEncoding('utf8'); // important

  conn.on('data', onConnData);
  conn.once('close', onConnClose);
  conn.on('error', onConnError);

  function onConnData(d) {
    console.log('connection data from %s: %j', remoteAddress, d);
   
    io.emit('chat message', d);
   
    try{conn.write(d);}catch(e){console.log(e)}
    
  }

  function onConnClose() {
    console.log('connection from %s closed', remoteAddress);
  }

  function onConnError(err) {
    console.log('Connection %s error: %s', remoteAddress, err.message);
  }
}


////////////////////////////////////////////////////
//////////////////////////////////////////////
//tcp send:


function sendToMax(inputstring)
  {
    var client = new net.Socket();
    client.connect(sendPORT, sendHOST, function() {

      console.log('sending "' + inputstring + '"TCP to: ' + sendHOST + ':' + sendPORT);
      // Write a message to the socket as soon as the client is connected, the server will receive it as message from the client 
      client.write(inputstring);
      client.destroy(); // if no response is to be awaited else: comment this line out!

    });

    client.on("error", function(e){console.log("could not connect to Max")})
  }
