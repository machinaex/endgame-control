*Boilerplate*
=============

Basis Code für alle machina ENDGAME Electron Apps.

App starten mit 
`$ npm start`  

*optional:* 

`$ npm start debug`  
startet app im Debug Modus.

`$ npm start db <host> <port>`  
überschreibt config/script einstallungen für Player Status Datenbank Connection

`$ npm start debug db <host> <port>`  
startet app in Debug Modus und mit angegebener Datanbank Connection.

* [App Aufsetzen](#markdown-header-aufsetzen)

	* [Dependencies](#markdown-header-dependencies)

	* [Dateien](#markdown-header-dateien)

* [Aktualisieren](#markdown-header-aktualisieren)

* [Benutzen](#markdown-header-benutzen)

	* [main](#markdown-header-main)

	* [player](#markdown-header-player)

	* [API](#markdown-header-api)

--------------------------------------------------------------

App Aufsetzen
==========

Den Boilerplate Ordner Kopieren und in eigenen Appnamen umbennen.

Dependencies
-------------

**ip** **mousetrap** und **rethinkdb** node Module im App Ordner installieren:  
`./<appname>$ npm install ip`  
`./<appname>$ npm install mousetrap`  
`./<appname>$ npm install rethinkdb`  
	
**rethinkdb** kann auch global installiert werden:  
`$ npm install -g rethinkdb`    

**electron** solllte global installiert werden:
`$ npm install -g electron`  


Dateien
--------

Die folgende Dateien sollten nun teil des App Projektes sein. Anpassungen in
*config.json* und *package.json* sind obligatorisch. 

###config.json

im file **config.json** muss definiert werden:

> "name": "<appname>"  
> "udpPort": "<port>"  
> "tcpPort": "<port>"  

*optional:*  
> "debug": "<true/false>"

debug true erlaubt aufruf der konsole und zeigt icons in der menüleiste.  

> "WINDOW":{<initialisierungsparameter für das BrowserWindow>}

wenn kein WINDOW in der config angegeben ist, wird das Window mittig mit Größe 800x800 initialisiert.

Bsp.:
> "WINDOW" : {"height": 600,"width": 600, "x":100, "y":400, "frame":false}  

positioniert das Fenster, stellt die Größe ein und entfernt den Rahmen (schließen icon etc.)


###package.json

das `"name"` Attribut muss in den Appnamen geändert werden.


###index.html

Wenn weitere script dateien hinugefügt werden, diese unterhalb der *boilerplate.js* anlegen:
```html

	<!-- do not change this line: -->
	<script type="text/javascript" src="boilerplate.js"></script>
	<!-- add your own java script files here: -->
	<script type="text/javascript" src="my_app.js"></script>
	<script type="text/javascript" src="more_app.js"></script>

```

###app.js

Beispielskript, das bereits in *index.html* eingebunden ist.

###main.js

hier sollten keine Änderungen vorgenommen werden. Datei wird mit neuer Version überschrieben.

###boilerplate.js

hier sollten keine Änderungen vorgenommen werden. Datei wird mit neuer Version überschrieben.

Aktualisieren
==============

*boilerplate.js* und *main.js* sollten mit jedem Update der Boilerplate in jeder App durch die neue Version ersetzt werden.

Benutzen
=========

Nützliche Funktionen, Funktionsaufrufe und Variablen, die in *boileplate.js* angelegt sind.

main
----------

Erlaubt Zugriff auf wichtige Funktionen, Events und Variablen in boilerplate.js, main.js und für die kommunikation mit dem app_handler.

#####config object
`main.config.<value>`  
JS Objekt, das die config.js daten enthält. (siehe oben)

Bsp.:
```javascript
console.log("app name is: " + main.config.name + ". udp port is: " + main.config.udpport + ".");
```


#####init function call
`main.init = function(<appdata>, <language>){}`  
Wird nach Login Prozess in login App oder bei Neustart der App aufgerufen, wenn der Login Prozess bereits abgeschlossen ist. 
appdata ist `undefined` wenn app nicht in Spieler Objekt gefunden wird. 
language ist **'en'** oder **'de'**

Bsp.:
```javascript
main.init = function(myappdata, mylanguage)
{
  console.log("init with appdata: " + myappdata + " and language: " + mylanguage);
}
```


#####receive function call
`main.receive = function(<appaction>){}`  
Wird aufgerufen, wenn app_handler Daten an App schickt die etwa durch einen cue oder eine andere lokale App gesendet wurden.

```javascript
main.receive = function(msg)
{
	console.log("new instruction: " + JSON.stringify(msg));
}
```

#####send function
`main.send(<data>)`  
Versendet data an app_handler. Kann WINDOW Property enthalten um eigene Fenstereigenschaften anzupassen.
Kann Anweisungen an lokale Apps auf dem gleichen Rechner enthalten.
Siehe die API der entsprechenden App um passende json zu senden.

Bsp.:
```javascript
// This will be send to remoteApp and otherRemoteApp
msgToRemoteApp = {"remoteApp":{"greetings":"hello other local app"}, "otherRemoteApp":"cheers"};
main.send(msgToRemoteApp);

// This will be send to every other app. myself too!
msgToAllOtherApps = {"all":"high Five all other apps!"};
main.send(msgToAllOtherApps);

// This will come back but with an individual "msg" value for myself
msgToAllOtherApps = {"all":{"msg":"high Five all other apps!"}, "myappname":{"msg":"high five myself"}};
main.send(msgToAllOtherApps);

// This will be minimizing and focussing my window
msgToMain = {"WINDOW": ["focus()","minimize()"]};
main.send(msgToMain);
```

#####send TCP function
`main.sendTCP(<url>, <port>, <message>)`  
TCP message versenden

#####send UDP function
`main.sendUDP(<url>, <port>, <message>)`  
UDP message versenden

#####commandline print
`main.print(<message>)`
leitet message an *main.js* console.log funktion weiter. Wird als `appname> message` in commandline angezeigt.

#####switch audio output
`main.speaker(<bool>);`  
Laptop Lautsprecher ein (true) oder aus (false) schalten.  

`main.headphones(<bool>);`  
Audio in Kopfhörer buchse ein (true) oder aus (false) schalten.

Der Status wird in 'hardware' property von Spieler eingetragen.  

Den Status der Audioeinstellungen abrufen:  
```javascript
player.get(["hardware"], function(data) {
		let speaker_status = data.hardware.speaker;
		let headphone_status = data.hardware.headphones;
		let audiojack_status = data.hardware.audiojack;

		console.log("speaker " + speaker_status);
		console.log("headphones " + headphone_status);
		console.log("audiojack " + audiojack_status);
	}
);
```
	

player
----------

Erlaubt Zugriff auf das dem Laptop zugeordnete Player Objekt in der Datenbank.

#####listen for changes
`player.getChanges(<callback>, <property (optional)>)`  
installiert change feed in Player Object. Änderungen werden an Callback Function übergeben.  
Zweites Argument kann eingenschaft des Player objektes sein. Dann wird Callback lediglich bei Änderungen in der Eigenschaft und auch nur die Eigenschaft selbst übergeben.

Bsp.:
```javascript
// Get changes in score property
player.getChanges(getscore, "score");

function getscore(score)
{
	console.log("My Score: " + score);
}

// Monitor different values in Player Object
player.getChanges(myChanges);

function myChanges(data)
{
	console.log("My Old Score: " + data["old_val"]["score"]);
	console.log("My Old Attributes: " + data["old_val"]["attributes"]);

	console.log("My New Score: " + data["new_val"]["score"]);
	console.log("My New Attributes: " + data["new_val"]["attributes"]);
}
```

#####set Appdata function
`player.setAppdata(<jsobject>)`  
erwartet jsobject mit Appdata. Ersetzt App spezifische appdata mit neuem jsobject.

#####get Appdata function
`player.getAppdata(<callback>)`  
callback Funktion wird aufgerufen wenn Appdaten aus Datenbank geholt wurden. Callback übergibt appdaten.

Bsp.:
```javascript
player.getAppdata(refreshAppdata)

function refreshAppdata(recentAppdata)
{
	myappdata = recentAppdata;
	console.log(JSON.stringify(myappdata))
}
```

#####add Attribute function
`player.addAttribute(<new_attribute>)`  
Fügt der attributes Liste in Spieler Objekt, wenn noch nicht vorhanden, neues Attribut hinzu.

#####remove Attribute function
`player.removeAttribute(<some_attribute>)`  
Entfernt, wenn vorhanden, existierendes Attribut aus attributes Liste im Spieler Objekt.

#####get function
`player.get([<list of properties>], <callback>)`  
Zeilen aus Player holen. Benötigt Liste von Strings und callback Funktion.

Bsp.:
```javascript
player.get(["score", "attributes"], myfunction);

function myfunction(properties)
{
	console.log("Score is: " + properties.score + " current Attributes are: " + properties.attributes);
}
```


#####change score function
`player.score(<value>)`  
Ändert die Score Property des Spielers um `<value>`. Wenn Value negativ ist, wird der Wert abgezogen.
Legt zudem eine appspezifische Score property in Appdaten an die bei 0 beginnt und den jeweiligen wert hinzufügt oder abzieht.



API
----------

*boilerplate.js* und *main.js* filtern Nachrichten an die App nach keywords, die die basisfunktionen betreffen.

#####Window property
ist optional: Hier kann als String notiert jede Methode aus der [electron browser window api](https://github.com/electron/electron/blob/master/docs/api/browser-window.md#instance-methods) eingefügt werden um das Fenster zu manipulieren.

Beachten:
Nach `hide()` sorgt der Aufruf `show()` dafür, dass das Window nicht in Fullscreen angezeigt wird.

Bsp:
`{"WINDOW":["setPosition(0,0)","focus()"]}` : Verschiebt das App Fenster an die Position 0,0 und fokussiert es.
oder
`{"WINDOW": ["focus()","minimize()"]}`

#####Screensaver property
`"SCREENSAVER":{}`  
ist optional und funktioniert entweder mit einfachen boolschen wie:  
`"SCREENSAVER":true` (Schwarzer Screen über App) oder  
`"SCREENSAVER":false` (Screensaver aus)  

oder mit Parametern:  
 `"SCREENSAVER":{"image":<linktoimage>, "opacity":<0. - 1.>, "html":<htmlstring>}`  
 alle parameter sind optional. Anwesenheit eines Parameters wird als *true* Befehl zum Anschalten des Screensavers interpretiert.  
 Ein leeres Objekt `"SCREENSAVER:{}` wird interpretiert wie *false* (Screensaver aus)

#####Boilerplate property
`"Boilerplate":"init"`  
Sorgt für aufruf der Main.init funktion in boilerplate.js.