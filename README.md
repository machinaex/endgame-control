# endgame control

Software und Daten, die auf dem Zentralrechner laufen um ENDGAME zum Leben zu erwecken.

*Achtung*: Da hier nicht ausreichend Speicherplatz dafür zur Verfügung steht sind die Umfangreichen Sounddateien ausgelagert. Zu ergänzen ist der Ordner Samples im [ENDGAME Liveset Project](sound/ENDGAME%20Liveset%20Project/). Die entsprechenden Dateien finden sich im ENDGAME Drive Ordner [sound](https://drive.google.com/open?id=1o_pk_3ULaupFkPShT1Umc7zLA0V27rFp)

###Dependencies

* [MaxMsp](http://www.cycling74.com)
* Python 2.7 (externe Module siehe [framework_server](framework_server/))
* Node.js
* Electron
* [D-Pro Licht Software](https://www.enttec.com/eu/products/controls/lighting-controller/d-pro/)
* Ableton Live