/*
*  define app specific properties in the variables below
*/
var chat = {
  "example": {
    "type": "string"
  }
};

var level0 = {

};

var login = {

};

var level1 = {

};

var website_app = {

};

var profil_app = {

};

var phone_app = {

};

var inventory = {

};

var playground = {
  "swal": {
    "type":"object",
    "description":"targets swal function.",
    "properties": {
      "level":{
        "type":"string",
        "enum":["troll", "fin", "befehl"]
      }
    }
  }
};

var fileexplorer = {

};

var questbook = {
  "command": {
    "type":"string",
    "enum":["checkbox", "html", "remove", "set"]
  },
  "tags": {
    "type":"array",
    "items": {
      "type":"string"
    }
  },
  "content": {
    "type":"array",
    "items": {
      "type":"string"
    }
  },
  "status": {
    "type":"string",
    "enum":["checked", "unchecked", "x"]
  }
};

var statusbalken = {
  "time": {
    "type":"string"
  }
};

var lightclient = {

};

var solitaire = {

};

var wolfenstein = {

};

var hacked = {
  "cmd": {
    "type":"string",
    "enum":["start","stop"]
  }
};

var all = {
  "speaker": {
    "type": "boolean",
    "format": "checkbox"
  },
  "headphones": {
    "type": "boolean",
    "format": "checkbox"
  }
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

var global_actions = {
  "WINDOW": {
    "type":"array",
    "format": "table",
    "title": "Window options",
    "uniqueItems": true,
    "items": {
      "type": "string",
      "title": "option"
      //"enum": ["focus()", "minimize()", "hide()", "show()"]
    },
    "default": [""]
  },
  "SCREENSAVER" : {
    "type":"object",
    "title": "Screensaver options",
    "properties": {
      "image": {
        "type": "string"
      },
      "opacity": {
        "type": "number"
      },
      "html": {
        "type": "string"
      }
    }
  },
  "Boilerplate" : {
    "type":"string",
    "title" : "Boilerplate options",
    "enum":["init"]
  },
  "active" : {
    "type": "boolean"
  }
};

// Is being populated when cues are loaded
var cuenames = [];
var listenernames = [];

var cue = {
  "title": "Cue",
  "headerTemplate": "{{ i1 }} {{ self.Name }}",
  "type": "object",
  "id": "a_cue",
  "properties": {
    "Name": {
      "required":true,
      "type": "string",
      "description": "Cue NAME Capital Letters, One Word",
      "propertyOrder": 1
    },
    "Comment": {
      "type": "string",
      "description": "Describe the Cue or when it is supposed to be triggered",
      "propertyOrder": 2
    },
    "Page": {
      "title": "Page Cuelist",
      "type": "array",
      "descripton":"Cues that will be listet when this cue is triggered",
      "format": "table",
      "propertyOrder": 3,
      "items": {
        "title": "Cues",
        "type": "string",
        "enum": cuenames
      }
    },
    "Index": {
      "title": "Index Order",
      "type":"number",
      "propertyOrder": 4,
      "description": "Property to manually order cues."
    },
    "Style": {
      "type":"string",
      "propertyOrder": 5,
      "description":"name of css class that is added to the cues visualization in control app",
      "enum":["auto", "win", "fail", "optional", "control"]
    },
    "Action": {
      "type": "array",
      "title": "App Actions",
      "propertyOrder": 5,
      "items": {
        "title": "Action",
        "type": "object",
        "properties": {
           "Target": {
              "type":"string",
              "required":true,
              "propertyOrder": 1,
              "description": "update or full query to define collection of targetet players"
            },
            "apphandler" : {
              "type": "object",
              "properties": global_actions
            },
            "chat": {
              "type":"object",
              "properties" : Object.assign({}, chat, global_actions)
            },
            "level0": {
               "type":"object",
               "properties" : Object.assign({}, level0, global_actions)
            },
            "level1": {
               "type":"object",
               "properties" : Object.assign({}, level1, global_actions)
            },
            "login": {
              "type": "object",
              "properties" : Object.assign({}, login, global_actions)
            },
            "profil_app": {
              "type":"object",
              "properties" : Object.assign({}, profil_app, global_actions)
            },
            "website_app": {
              "type":"object",
              "properties" : Object.assign({}, website_app, global_actions)
            },
            "phone_app": {
              "type":"object",
              "properties" : Object.assign({}, phone_app, global_actions)
            },
            "questbook": {
              "type":"object",
              "properties" : Object.assign({}, questbook, global_actions)
            },
            "statusbalken": {
              "type":"object",
              "properties" : Object.assign({}, statusbalken, global_actions)
            },
            "lightclient": {
              "type":"object",
              "properties" : Object.assign({}, lightclient, global_actions)
            },
            "inventory": {
              "type":"object",
              "properties" : Object.assign({}, inventory, global_actions)
            },
            "playground": {
              "type":"object",
              "properties" : Object.assign({}, playground, global_actions)
            },
            "fileexplorer": {
              "type":"object",
              "properties" : Object.assign({}, fileexplorer, global_actions)
            },
            "solitaire": {
              "type":"object",
              "properties" : Object.assign({}, solitaire, global_actions)
            },
            "wolfenstein": {
              "type":"object",
              "properties" : Object.assign({}, wolfenstein, global_actions)
            },
            "hacked": {
              "type":"object",
              "properties" : Object.assign({}, hacked, global_actions)
            },
            "all": {
              "type": "object",
              "properties" : Object.assign({}, all, global_actions)
            }
         }
       },
       "default": [
        {
          "Target": "''"
        }
      ]
    },
    "Control": {
      "type": "array",
      "title": "Control Message",
      "propertyOrder": 6,
      "items": {
        "title": "Control",
        "type":"object",
        "properties" : {
          "Target": {
            "type": "string",
            "required": true
          },
          "lock": {
            "type":"boolean"
          },
          "inactive": {
            "type":"boolean"
          },
          "inputs" : {
            "type": "boolean"
          },
          "screen": {
            "type": "boolean"
          },
          "headphones": {
            "type": "boolean"
          },
          "speaker": {
            "type": "boolean"
          },
          "npm": {
            "type": "string"
          }
        }
      },
      "default": [
        {
          "Target": "''"
        }
      ]
    },
    "Listen": {
      "type": "array",
      "title": "Feed Listeners",
      "propertyOrder": 7,
      "items": {
        "title": "Listener",
        "headerTemplate": "{{ i1 }} {{ self.Name }}",
        "type":"object",
        "properties": {
          "Name": {
            "type":"string",
            "required":true
          },
          "Target": {
            "type": "string",
            "description": "update or full query to define collection of targetet players to install changefeed"
          },
          "Condition": {
            "description": "python assignment. Followcue is dispatched if condition is TRUE",
            "type":"string",
            "required":true,
          },
          "Query": {
            "type": "string"
          },
          "Followcue": {
            "required": true,
            "type":"string",
            "enum": cuenames
          },
          "Type": {
            "type":"string",
            "enum":["close_on_match", "infinite"],
            "default":"close_on_match"
          }
        },
        "default":
        {
          "Condition": "True",
          "Followcue": ""
        }
      },
      "default": [
        {
          "Condition": "True",
          "Followcue": ""
        }
      ]
    },
    "Change": {
      "type": "array",
      "title": "Player Status Changes",
      "propertyOrder": 8,
      "items": {
        "title": "Change",
        "headerTemplate": "",
        "type": "object",
        "properties": {
          "Target": {
            "type": "string",
            "description": "update or full query to define collection of targetet player objects"
          },
          "Update": {
            "type": "string"
          },
          "Query": {
            "type": "string"
          }
        }
      }
    },
    "Close": {
      "type": "array",
      "title": "Close Listener",
      "description": "Define Listeners (by name) to be closed",
      "format": "table",
      "propertyOrder": 9,
      "items": {
        "type": "string",
        "title": "Close",
        "enum":listenernames
      }
    },
    "Execute": {
      "description": "Not yet implemented",
      "title": "Execute Functions",
      "propertyOrder": 10,
      "type": "array",
      "format": "table",
      "items": {
        "title": "Execute",
        "type": "string"
      }
    }
  }
};