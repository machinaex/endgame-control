/*
* TODO: Validate hinzufuegen
*				Import /Export hinzufuegen
*				Liste von Template Querys daneben anzeigen
* 			Cues für Close, Page Turn und Followcue als dropdown aus liste sammeln (enum)
*
*/
//JSONEditor.defaults.options.iconlib = 'fontawesome4';
JSONEditor.defaults.theme = 'bootstrap2';
JSONEditor.defaults.iconlib = 'fontawesome3';
//window.$ = window.jQuery = require('/home/lasse/Documents/javascript/cue_maker/node_modules/jquery/dist/jquery.min.js');
var editor;
var selected_cue;
var do_update = false; // Wether or not to update DB all the time
var table_name = "";
var primary_key = "";

// JSONEditor.defaults.options.disable_array_delete = true;

var submitBtn = document.getElementById('submit');
var bckpBtn = document.getElementById('backup');

var update_checkbox = document.getElementById('db_update');

var allow_delete = false;

bckpBtn.disabled = true;
submitBtn.disabled = true;

update_checkbox.addEventListener('click',function() {
	do_update = update_checkbox.checked;
	}
);

/*
* Replaces the whole Table on DB with current json
*/
submitBtn.addEventListener('click',function() 
	{
		// Validate the editor's current value against the schema
		var errors = editor.validate();

		if(errors.length) {
		  console.error("Could not Submit Table. Invalid JSON: ");
		  console.error(errors);
		}
		else {
		  replaceTable(editor.getValue());
		  submitBtn.disabled = true; // Allow Submitting
		}
	}
);

bckpBtn.addEventListener('click',function() 
	{
		bckpBtn.disabled = true;
		archiveTable();
	}
);

document.getElementById('load_player').addEventListener('click',function() 
	{
		table_name = "player";
		primary_key = "ip";
		player.connect( function(connection) 
	     {
	       r.db("storage").table("player").orderBy({index: 'ip'}).run(connection, function(err, cursor){
	       		if (err) throw err;
        		cursor.toArray(function(err, result) {
            	if (err) throw err;
            	//console.log(JSON.stringify(result, null, 2));
            	showEditor(result, "Player", plyr);
        		});
	       }
	       );
	     }
	  );
	}
);

document.getElementById('load_cues').addEventListener('click',function() 
	{
		table_name = "cues";
		primary_key = "Name";
		player.connect( function(connection) 
	     {
	       r.db("storage").table("cues").orderBy({index: 'Name'}).run(connection, function(err, cursor){
	       		if (err) throw err;
        		cursor.toArray(function(err, result) {
            	if (err) throw err;
            	//console.log(JSON.stringify(result, null, 2));
            	showEditor(result, "Cues", cue);
        		});
	       }
	       );
	     }
	  );
	}
);

function populate(cuelist)
{
	cuenames.length = 0;
	listenernames.length = 0;
	listenernames[0] = 'all';
	
	cuelist.forEach(function(cue_i, i)
		{
			cuenames.push(cue_i.Name);
			if(cue_i.Listen != undefined)
			{
				for(listener of cue_i.Listen)
				{
					if(listener.Name != undefined)
					{
						listenernames.push(listener.Name);
					}
				}
			}
		}
	);
}

function showEditor(cuelist, schema_title, item_schema)
{
	if (document.getElementById('allow_delete').checked)
	{
		allow_delete = true;
		// JSONEditor.defaults.options.disable_array_delete = false;
		update_checkbox.checked = false;
		update_checkbox.disabled = true;
	} else {
		allow_delete = false;
		// JSONEditor.defaults.options.disable_array_delete = true;
		update_checkbox.disabled = false;
	}
	do_update = update_checkbox.checked;

	bckpBtn.disabled = false;

	
	if (editor != undefined)
	{
		editor.destroy();
	}

	// Populate cuenames list and listener names list
	populate(cuelist);
	
	editor = new JSONEditor(document.getElementById('editor_holder'),
		{ 
			schema : {
				"title": schema_title,
				"type": "array",
				"format": "tabs",
				"items" : item_schema,
				"options": {
					"disable_array_delete": !allow_delete,
					"disable_array_reorder": true
				}
			}, 
			required_by_default: false, 
			display_required_only: true
			//theme: "bootstrap2"
			//iconlib: "fontawsome4"
		}
	);
	editor.setValue(cuelist);

	// Checks if the number of cues increased (cue added) or decreased (cue removed)
	editor.on('change', function() {
			let cueamount = editor.getValue().length;

			if (cueamount < cuelist.length)
			{
				//console.log("cue removed")
			} else if (cueamount > cuelist.length)
			{
				let lastcue_path = 'root.'+ (cueamount - 1);
				monitor_cue_change(cueamount-1);
			} 
		}
	);
	cuelist.forEach(function(cue_i, i)
		{
			monitor_cue_change(i);
		}
	);
}

/*
*  Checks for Changed Properties and pushes them to the DB
*  If Name Property is changed, cue on DB is replaced by copy with new name.
*/
function monitor_cue_change(i)
{

	var cuetab_path = 'root.' + i;
	var cuetab_name_path = cuetab_path + '.' + primary_key;
	// console.log(cuetab_path);

	// Watch for changes in Cue Name Property
	editor.watch(cuetab_name_path, function()
		{
			//console.log("New Cue: " + cuetab_name_path);
			let cuetab = editor.getEditor(cuetab_path);
			//console.log("New Cue Value: " + JSON.stringify(cuetab.getValue()));

			if(do_update)
			{
				removeObject(cuetab.getValue());
			} else {
				submitBtn.disabled = false; // Allow Submitting
			}
			
			editor.watch(cuetab_path, add_when_changed = function()
				{
					if (do_update)
					{
						addObject(cuetab.getValue());
					}
					editor.unwatch(cuetab_path, add_when_changed);
				}
			);
		}
	)

	// Watch for changes in Cue Properties
	editor.watch(cuetab_path, function()
		{
			//console.log(cuetab_path);
			let cuetab = editor.getEditor(cuetab_path);
			//console.log(cuetab.getValue());
			if(do_update)
			{
				replaceObject(cuetab.getValue());
			} else {
				submitBtn.disabled = false; // Allow Submitting
			}
		}
	)
}




function replaceObject(cueJSON)
{
	let buff = {};
	player.connect( function(connection) 
     {
       r.db("storage").table(table_name).insert(cueJSON, {conflict: "replace"}).run(connection, function(err, result){
         player.getResult(err, result, function(data) {
          if (data.replaced)
	         	{
	         		console.log("Replaced " + cueJSON[primary_key] + " in " + table_name);
	         	}
         }, connection)
       }.bind(player)
       );
     }.bind(player)
  );
}

function removeObject(cueJSON)
{
	player.connect( function(connection) 
	     {
	       r.db("storage").table(table_name).get(cueJSON[primary_key]).delete().run(connection, function(err, result){
	         player.getResult(err, result, function(data) {
	          if (data.deleted)
	         	{
	         		console.log("Removed " + cueJSON[primary_key] + " from " + table_name);
	         		//console.log(cueJSON);
	         	}
	         }, connection)
	       }.bind(player)
	       );
	     }.bind(player)
	  );
}
function addObject(cueJSON)
{
	player.connect( function(connection) 
	     {
	       r.db("storage").table(table_name).insert(cueJSON).run(connection, function(err, result){
	         player.getResult(err, result, function(data) {
	         	if (data.inserted)
	         	{
	         		console.log("Added " + cueJSON[primary_key] + " in " + table_name);
	         		console.log(cueJSON);
	         	} else {
	         		console.log(data)
	         	}
	         }, connection)
	       }.bind(player)
	       );
	     }.bind(player)
	  );
}

/*
* Replace whole Table with current
*/
function replaceTable(tableJSON)
{
	console.log("Replacing " + table_name);
	console.log(tableJSON);
	player.connect( function(connection) 
    {
    	let datetime = new Date();
			let storename = "storage_" + table_name + "_" + datetime.getFullYear() + "_" + datetime.getMonth() + "_" + datetime.getDate() + "_" + datetime.getHours() + "_" + datetime.getMinutes() + "_" + datetime.getSeconds();

			r.db("archive").tableCreate(storename, {primaryKey: primary_key}).run(connection, function(err, result) {
						if(err)
				    {
				      console.error(err);
				      connection.close();
				    } else {
				    	r.db("archive").table(storename).insert(r.db("storage").table(table_name)).run(connection, function(err, result) {

				    	console.log("Created backup in archive: " + storename)
				    	r.db("storage").tableDrop(table_name).run(connection, function(err, result){
			        		if(err)
							    {
							      console.error(err);

							      connection.close();
							    } else  {
			          			r.db("storage").tableCreate(table_name, {primaryKey: primary_key}).run(connection, function(err, result){
		         							if(err)
											    {
											      console.error(err);
											      connection.close();
											    } else  {
		         								r.db("storage").table(table_name).insert(tableJSON).run(connection, function(err, result)
		         									{
		         										player.getResult(err, result, function(data) {
		         												if (data.inserted)
		         												{
		         													console.log(table_name + " replaced")
		         													console.log(data);
		         												} else {
		         													console.log(data);
		         												}
		         											}.bind(player), connection
		         										);
		         									}.bind(player)
		         								);
		         							}
			       						}.bind(player)
			       					);
			     					}
									}.bind(player)
								);
					    }.bind(player)
					  );
					}
				}.bind(player)
			); 
		}.bind(player)
	);
}

/*
*  Put copy of Table to archive
*/
function archiveTable()
{
	player.connect( function(connection) 
    {
    	let datetime = new Date();
			let storename = "storage_" + table_name + "_" + datetime.getFullYear() + "_" + datetime.getMonth() + "_" + datetime.getDate() + "_" + datetime.getHours() + "_" + datetime.getMinutes() + "_" + datetime.getSeconds();

			r.db("archive").tableCreate(storename, {primaryKey: primary_key}).run(connection, function(err, result) {
					if(err)
			    {
			      console.error(err);
			      connection.close();
			    } else {
			    	r.db("archive").table(storename).insert(r.db("storage").table(table_name)).run(connection, function(err, result) {
			    			console.log("Created backup in archive: " + storename)
			    			bckpBtn.disabled = false;
			    		}
			  		);
			  	}
		  	}
			);
		}
	);
}

