var net = require('net'); // for tcp
var dgram = require('dgram'); // for udp
const electron = require('electron');
const {remote} = require('electron');
var myip = require('ip'); // to find out my ip

var r = require('rethinkdb');

// Instance of Main Prototype that enables App to communicate
var main = new Main()

// Provide Connection Properties to Rethink DB Commendline before config
for(var i = 0; i < electron.remote.process.argv.length; i++)
{
  if (electron.remote.process.argv[i] == "db")
  {
    if (electron.remote.process.argv[i+2] != undefined)
    {
      var player = new Player(electron.remote.process.argv[i+1], electron.remote.process.argv[i+2]);
    } else {
      main.print("db arg but no host and/or port provided");
    }
  }
}
if (player == undefined)
{
  if (main.config.rethink != undefined)
  {
    var player = new Player(main.config.rethink.ip, main.config.rethink.port);
  }
  else
  {
    // defaulting to endgame live setting
    var player = new Player("192.168.0.2", 28015);
  }
}

// Run on App start
main.begin();

try {
  var mousetrap = require('mousetrap'); // To build custom key bindings

  Mousetrap.bind("ctrl+shift+p", function(){
    main.send({"WINDOW": ["minimize()"]});
  });
} catch(e) {
  console.error("Problem using moustrap keybindings: " + e);
}

/* Event: fromMain
*  listener for message/event from main.js through electron.ipcRenderer
*/
electron.ipcRenderer.on('fromMain', (event, message) => {

  console.log(message); // Prints whatever comes  in

  if(message.ALLOWSCROLLING != undefined)
  {
    let body = document.getElementsByTagName("BODY")[0];
    if(message.ALLOWSCROLLING)
    {
      body.style.overflow = "auto";
    }
    else
    {
      body.style.overflow = "hidden";
    }

  }
  if (message.SCREENSAVER == true)
  {screensaver(true);}
  if(message.SCREENSAVER == false)
  {screensaver(false);}
  if(typeof message.SCREENSAVER === 'object')
  {
    if (isEmpty(message.SCREENSAVER))
    {
      screensaver(false);
    } else
    {
      screensaver(true,message.SCREENSAVER.image,message.SCREENSAVER.opacity,message.SCREENSAVER.html)
    }
  }

  // check for Boilerplate specific messages
  if (message.Boilerplate != undefined)
  {
    if (message.Boilerplate == "init")
    {
      main.begin();
    }
  }

  // make incomming messages from main available
  if (typeof main.receive === "function")
  {
    main.receive(message);
  } else {
    console.error("There is no receive function defined. See boilerplate Readme for details.");
  }
})

function Main()
{
  // Try to implement properties from config.json
  try{
    this.config = require('./config.json'); //with path
  }
  catch(e){
    this.config = undefined;
    console.error("Boilerplate: Your Config file config.json seems to be broken");
  }

  /* Function: Main.print
  *  Forward message to main.js console.
  *  Is displayed in commandline
  */
  this.print = function(message)
  {
    electron.ipcRenderer.send("print", message);
  }

  /* Function: Main.begin
  *  Requires Player Data from rethink DB.
  *  When loaded, forwards to hand init on to app specific function.
  */
  this.begin = function()
  {
    try
    {
      //player.getAppdata(this.onAppdataBegin.bind(this));
      player.get([this.config.name, "language"], this.onPlayerDataAvailable.bind(this));
    }
    catch(e)
    {
      console.error("Boilerplate can not run init: " + e);
    }
  }

  /* Funciton: preinit
  *  called when appdata returns from DB.
  *  hands appdata to main for WINDOW specific settings
  *  appdata is then forwarded to app specific init function
  *  Will not forward if there is no language set in Player Object
  */
  this.onPlayerDataAvailable = function(playerdata)
  {
    if (playerdata[this.config.name] != null)
    {
      electron.ipcRenderer.send("handling", playerdata[this.config.name]);
    }
    else
    {
      console.error("App Data for " + this.config.name + " is " + playerdata[this.config.name]);
    }

    if(playerdata["language"] != "none" && playerdata["language"] != undefined)
    {
      if (typeof this.init === "function")
      {
        this.init(playerdata[this.config.name], playerdata["language"]);
      } else {
        console.error('There is no init function defined. See boilerplate Readme for details.');
      }
    } else {
      console.log("Player language is " + playerdata["language"] + ". Maybe game has not startet yet");
    }
  }

  /* Function: main.send
  *  Forward messages to main.js
  */
  this.send = function(input)
  {
    electron.ipcRenderer.send("forwarding",input);
  }

  this.sendTCP = function(url,port,message)
  {
    var client = new net.Socket();
    client.connect(port, url, function() {

      console.log('sending "' + message + '"TCP to: ' + url + ':' + port);
      // Write a message to the socket as soon as the client is connected, the server will receive it as message from the client
      client.write(JSON.stringify(message));
      client.destroy(); // if no response is to be awaited else: comment this line out!

    });
  }

  this.sendUDP = function(url,port,message)
  {
    var message = new Buffer(JSON.stringify(message));

    var client = dgram.createSocket('udp4');
    client.send(message, 0, message.length, port, url, function(err, bytes) {
        if (err) throw err;
        // console.log('UDP message sent to ' + port +':'+ url);
        client.close();
    });

  }

  /*
  * Switch Headphones and Speaker on and of and keep track of current state
  */
  this.headphones = function(toggle)
  {
    toggle_headphones = {"headphones":toggle};
    this.sendTCP("localhost", 5005, toggle_headphones);
    player.setProperty("hardware", toggle_headphones);
  }
  this.speaker = function(toggle)
  {
    toggle_speaker = {"speaker":toggle};
    this.sendTCP("localhost", 5005, toggle_speaker);
    player.setProperty("hardware", toggle_speaker);
  }
}



/*
* Function: screensaver
* switch overlay on/off
*/
function screensaver(on,image=undefined,opacity=1,html=undefined )
{
  if(on)
  {
    if(image != undefined)
    {
      image = "url("+image+")";
    }
    else
    {
      image = "none";
    }
    var HTML = '<div id="screensaver" style="margin-left:0px;margin-top:0px;position:absolute;min-width:100%;min-height:100%;opacity:'+opacity+';z-index:100;background:#000; z-index:100;background-image:'+image+'"></div>';

    document.body.innerHTML += HTML;

    if (html != undefined)
    {
      var element = document.getElementById("screensaver")
      element.innerHTML = html;
      //console.log(element);
    }

  }
  else
  {
    try{
        var element = document.getElementById("screensaver");
        element.parentNode.removeChild(element);
      }
    catch(e){
      console.log("got screensaver false but there is no screensaver on.")
    }
  }

}

/*
* Function: isEmpty
* checks if js Object is empty then returns true
*/
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

/* Constructor: Player
*  Player Prototype constructor
*/
function Player(host, port)
{
  // Where to find the rethink DB
  this.host = host;
  this.port = port;

  console.log("Access Player Data at " + host + ":" + port);

  /* Function: Player.getChanges
  *  start listeneing to changes in player status
  *  optional arg: property - only return property if specific property changed
  */
  this.getChanges = function(callback, property=undefined)
  {
    player.connect(function(conn) {
      r.db("status").table("player").get(myip.address()).changes().run(conn, function(err, cursor) {
        if (err) throw err;
        cursor.each(function(err, row) {
          if (err) throw err;
          if(property == undefined)
          {
            callback(row["new_val"]);
          }
          else if (row["old_val"][property] != row["new_val"][property])
          {
            callback(row["new_val"][property])
          }
        });
      })
    })
  }

  /* Function: Player.addAttribute
  *  Append a new entry in list of attributes
  *
  */
  this.addAttribute = function(new_attribute)
  {
    this.connect( function(conn)
      {
        r.db("status").table("player").get(myip.address()).update({ attributes:r.row("attributes").setInsert(new_attribute)} ).run(conn, function(err, result)
          {
            this.getResult(err,result,function(data){
                if(data["replaced"] == 1)
                {
                  console.log("Added Attribute: " + new_attribute);
                }
                else if (data["unchanged"] == 1)
                {
                  console.log("Attribute already exists: " + new_attribute);
                }
              }, conn
            )
          }.bind(this)
        );
      }.bind(this)
    );
  }

  /* Function: Player.removeAttribute
  *  Delete an existing entry in list of attributes
  *
  */
  this.removeAttribute = function(del_attribute)
  {
    this.connect( function(conn)
      {
        r.db("status").table("player").get(myip.address()).update({ attributes:r.row("attributes").difference([del_attribute])} ).run(conn, function(err, result)
          {
            this.getResult(err,result,function(data) {
                if(data["replaced"] == 1)
                {
                  console.log("Removed Attribute: " + del_attribute);
                }
                else if (data["unchanged"] == 1)
                {
                  console.log("No Attribute to remove: " + del_attribute);
                }
              }, conn
            )
          }.bind(this)
        );
      }.bind(this)
    );
  }

  /* Function: Player.setProperty
  *  update any property of Player Object
  */
  this.setProperty = function(property, data)
  {
    var update = {};
    update[property] = data;
    this.connect( function(connection)
      {
        r.db("status").table("player").get(myip.address()).update( update ).run(connection, function(err, result){
          this.getResult(err, result, function(data) {
            if(data["replaced"] == 1)
              {
                console.log(property + " updated: " + JSON.stringify(data));
              }
          }, connection)
        }.bind(this)
        );
      }.bind(this)
    );
  }
  /* Function: Player.score
  *  change main player score AND app specific score
  */
  this.score = function(value)
  {
    let score_update = {score:r.row("score").default(0).add(value)};
    this.update(score_update, "score")

    let appscore_update = {};
    appscore_update[main.config.name] = {score:r.row(main.config.name)("score").default(0).add(value)};
    this.update(appscore_update, "app score");

    console.log("change score by: " + value);
  }

  /* Function: Player.update
  *  do update based on js object
  */
  this.update = function(jsobject, description="update")
  {
    this.connect( function(connection)
      {
        r.db("status").table("player").get(myip.address()).update( jsobject ).run(connection, function(err, result){
          this.getResult(err, result, function(data) {
            if(data["replaced"] == 1)
              {
                console.log(description + " replaced" );
              } else {
                console.log(data);
              }
          }, connection)
        }.bind(this)
        );
      }.bind(this)
    );
  }
  /* Function: Player.setAppdata
  *  Replaces the appdata in Player status
  */
  this.setAppdata = function(new_appdata)
  {
    this.setProperty(main.config.name, new_appdata);
  }

  /* Function: Player.connect
  *  Create Connection to Status Database in Endgame Rethink Server
  *  Calls queryfunction when connection is established
  */
  this.connect = function(queryfunction)
  {
    return r.connect( {host: this.host, port: this.port}, function(err, conn) {
            if (err)
            {
              console.log("Could not connect to Database: " + err);
            } else {

              //connection = conn;
              queryfunction(conn)
            }

      })
  }

  /* Function: Player.getAppdata
  *  Fetch Current App specific Data from status
  *  Calls callback with exactly only the appdata (no "appname:")
  */
  this.getAppdata = function(callback)
  {
    this.get(main.config.name, function(data){
        callback(data[main.config.name])
      });
  }

  /* Function: Player.get
  *  Fetch specific row in my player
  *  Must be List of Properties.
  *  callback is called with JSON Object that addresses each property with its name.
  */
  this.get = function(property, callback)
  {
    //var connection = null;

    this.connect( function(connection)
      {
        this.getProperty(property, callback, connection)
      }.bind(this)
    );
  }

  /* Function: Player.getProperty
  *
  */
  this.getProperty = function(property, callback, conn)
  {
    r.db("status").table("player").get(myip.address()).pluck(property).run(conn, function(err, result){
        this.getResult(err,result,callback, conn)
      }.bind(this) );
  }

  /* Function: Player.getResult
  *  second argument for rethink .run() function. Has to be nested in function to access arguemnts
  *  Calls callback when ReQL could grab data
  */
  this.getResult = function(err, result, callback, connection)
  {
    if(err)
    {
      console.error("Boilerplate failed to get Player " + myip.address());
      connection.close();
    } else {
      try
      {
        var data = result;
        connection.close(function(err)
          {
            if (err)
            {
              throw err;
            } else {
              if (typeof callback === "function")
              {
                callback(data);
              } else {
                console.error("No Function found to return DB feedback")
              }
            }
          }
        );
        // TODO: if( callback.type === function) else "NOOOO"
        // callback(data);
      }
      catch(e)
      {
        console.log("Error In: Player " + myip.address() + " " + e);
        connection.close();
      }
    }
  }
}
