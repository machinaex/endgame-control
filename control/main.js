var net = require('net'); // for tcp
var dgram = require('dgram'); // for udp

var DEBUG = false; // set true when in testing mode

try {
	var config = require('./config.json'); //with path
}
catch(e){
	console.error("Your config file config.json is broken");
}

// Define Debug Mode. Command Line before Config.
for(var argv of process.argv)
{
	if (argv == "debug")
	{
		DEBUG = true;
	}
}
if (DEBUG == false)
{
	if (config.debug != undefined)
	{
		DEBUG = config.debug;
	} else {
		console.log("no debug property in config.json");
	}
}

console.log("DEBUG is " + DEBUG);


const {app,BrowserWindow,Menu,globalShortcut} = require('electron');
const path = require('path');
const electron = require('electron');
//const app = electron.app;
//const BrowserWindow = electron.BrowserWindow;


////////////////////////////////////////////////
//// COMMUNICATION WITH RENDERER > /////////////

/* Event: electron.ipcMain.on
*  Listen on messages from Boilerplate script
*	 Check for Window actions and forward to apphandler
*/
electron.ipcMain.on('forwarding', (event, arg) => {
      
    console.log("Main send: " + arg)  // prints "ping"

    // auf WINDOW inhalte ueberpruefen und anwenden
    let forwarddata = handleAppcommands(arg);

    if(forwarddata != undefined)
    {
    	sendUDP("localhost", 8001, JSON.stringify(forwarddata));
    }
	}
)

/* Event: electron.ipcMain.on
*  Handle Boilerplate script with appdata
*  Do NOT forward to apphandler
*/
electron.ipcMain.on('handling', (event, arg) => {
      
      console.log("Main handle: " + arg)  // prints "ping"

      // auf WINDOW inhalte ueberpruefen und anwenden
      handleAppcommands(arg);
 	}
)

/* Event: print
*	 Forwards message to console.log. 
*	 In main.js this is displayed in commandline.
*/
electron.ipcMain.on('print', (event, arg) => {
      
      console.log(config.name + "> " + arg)
 	}
)

/* Function: sendToRenderer
*  Forward message to app script
*/
function sendToRenderer(message)
{
	mainWindow.webContents.send('fromMain', message);
}


//// < COMMUNICATION WITH RENDERER  ///////////
///////////////////////////////////////////////

// App schliessen wenn window close befehl bekommen
app.on('window-all-closed', () => {
	app.quit()
});

app.on('ready', () => {

	startUDPServer();
	startTCPServer();

	// first register, then disable Global shortcuts
	// Seit Betatest vom 23.November für jede App immer angewendet!!
	// if (!DEBUG)
	if(true)
	{
		globalShortcut.register('Ctrl+Q',() => {
			console.log("Ctrl Q is disabled")});
		globalShortcut.register('Ctrl+W',() => {
			console.log("Ctrl W is disabled")});
		globalShortcut.register('Ctrl+X',() => {
			console.log("Ctrl X is disabled")});
		globalShortcut.register('Ctrl+M',() => {
			console.log("Ctrl M is disabled")});
	}

	let windowconf = {height: 800,width: 800}
	if (config.WINDOW != undefined){windowconf = config.WINDOW;}
	mainWindow = new BrowserWindow(windowconf)
	if (!DEBUG)
  {
		mainWindow.setMenu(null);
	}

	let url = require('url').format({
		protocol: 'file',
		slashes: true,
		pathname: require('path').join(__dirname, 'index.html')
	})

	mainWindow.loadURL(url);
	setMainMenu();

	app.on('browser-window-created',function(e,window) {
      window.setMenu(null);
  	});

})


function setMainMenu() {
  const template = [
    {
      //label: 'Filter',submenu: [{label: 'Hello',accelerator: 'Shift+CmdOrCtrl+H',click() {console.log('Oh, hi there!')}}]
    }
  ];
  if (!DEBUG)
  {
  	Menu.setApplicationMenu(Menu.buildFromTemplate(template));
  }
}

/* Funciton: handleAppcommands
*	 Evaluates json for WINDOWS commands.
*  Then returns command data w/out WINDOW Property
*/
function handleAppcommands(data)
{
	try {
		// data = JSON.parse(data);

		if (data.WINDOW != undefined)
		{
			console.log("setting BrowserWindow");

			data.WINDOW.forEach(function(command)
			{
				console.log(command);
				if (command.startsWith("allowScrolling"))
				{
					sendToRenderer(command);
				}
				else
				{
					try{
						resp = eval("mainWindow." + command);
						console.log(command + " : " + resp);
						if (resp != undefined) { 
							sendToRenderer({"WINRESPONSE":resp}) 
						}
					}	
					catch(e){
						console.log(e);
					}	
				}
			})
			delete data.WINDOW;
		}
	}
	catch(e)
	{
		console.error("Main could not set browser window: " + e)
		data = undefined;
	}

	// dont return empty Objects. Set them to undefined.
	if (Object.keys(data).length === 0 && data.constructor === Object)
	{
		return undefined
	}
	else
	{
		return data;
	}
}


///////////////////////////////////////
/// Network Receive Servers to receive messages from the AppHandler:

function startUDPServer()
{
	var port = config.udpPort;
	var host = '0.0.0.0';
	var server = dgram.createSocket('udp4');
	server.on('listening', function(){
		var address = server.address();
		console.log("UDP server listening on "+ address.address +":"+port)
	});
	server.on("message", function (message, remote) {
		console.log("UDP: " + remote.address + ":" + remote.port + " " + message);
		
		//message = String(message);
		message = JSON.parse(message);
		message = handleAppcommands(message);

		mainWindow.webContents.send('fromMain', message);
	});
	server.bind(port, host);
}



function startTCPServer()
{
	var server = net.createServer();  
	server.on('connection', handleConnection);

	server.listen(config.tcpPort, "0.0.0.0", function() {  
	  console.log('TCP server listening to %j', server.address());
	});

	function handleConnection(conn) {  
	  var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
	  console.log('new TCP client connection from %s', remoteAddress);

	  conn.setEncoding('utf8'); // important

	  conn.on('data', onConnData);
	  conn.once('close', onConnClose);
	  conn.on('error', onConnError);

	  function onConnData(message) {
	    //console.log('TCP: %s: %j', remoteAddress, message);
	    message = JSON.parse(message);
	    message = handleAppcommands(message);
	    
	    // dispatch netReceive event for other parts of the program to know about it
	    if (message != undefined)
	    {
	    	mainWindow.webContents.send('fromMain', message);
	    }
	    
	    // Evaluate message if contains target list
	    if (message.targets != undefined)
	    {
	    	conn.write("success"); // respond to validate
	    } else {
	    	conn.write("error");
	    }	    
	  }

	  function onConnClose() {
	    // console.log('connection TCP from %s closed', remoteAddress);
	  }

	  function onConnError(err) {
	    console.log('Connection TCP %s error: %s', remoteAddress, err.message);
	  }
	}
}




function sendTCP(url,port,message)
{
	var client = new net.Socket();
	client.connect(port, url, function() {

		console.log('TCP send "' + message + '" to: ' + url + ':' + port);
		// Write a message to the socket as soon as the client is connected, the server will receive it as message from the client 
		client.write(message);
		client.destroy(); // if no response is to be awaited else: comment this line out!
	});
}


function sendUDP(url,port,message)
{
	var message = new Buffer(message);

	var client = dgram.createSocket('udp4');
	client.send(message, 0, message.length, port, url, function(err, bytes) {
	    if (err) throw err;
	    console.log('UDP message sent to ' + port +':'+ url);
	    client.close();
	});

}


