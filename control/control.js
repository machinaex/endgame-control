jq = require('jquery');

/*
* DOM Elements
*/
var ctrl_panel = document.getElementById("ctrl_panel");
var tabs = document.getElementById("tabs");
var recent_cue = document.getElementById("recent");
var feedback_field = document.getElementById("feedback_field");

var visible_cues = [];
var tab_buttons = [];

window.onload = function()
{
	load_table("storage", "cues", r.asc('Index'), create);
}

// Static buttons
document.getElementById('load_all').addEventListener('click',function() 
	{
		load_table("storage", "cues", {index: "Name"}, list);
	}
);

document.getElementById('reload').addEventListener('click',function() 
	{
		load_table("storage", "cues", r.asc('Index'), create);
	}
);
document.getElementById('clear').addEventListener('click',function() 
	{
		clear_feedback();
	}
);

main.receive = function(message)
{
	if(message.Name != undefined)
	{
		console.log("Incomming Cue: " + message.Name);
		//feedback(message.Name);
	} 
	else if (message.feedback != undefined)
	{
		feedback(message.feedback)
	}
	

	show(message);
}

function feedback(obj)
{
	if (obj.success)
	{
		if (Array.isArray(obj.succes))
		{
			for (success of obj.success)
			{
				jq(feedback_field).prepend("<div class='success'>" + success + "</div>");

				recent_cue.innerHTML = success;
			}
		} else {
			jq(feedback_field).prepend("<div class='success'>" + obj.success + "</div>");

			recent_cue.innerHTML = obj.success;
		}
	}

	if (obj.failed)
	{
		if (Array.isArray(obj.failed))
		{
			for (fail of obj.failed)
			{
				jq(feedback_field).prepend("<div class='failed'>" + fail + "</div>");

				recent_cue.innerHTML = fail;
			}
		} else {
			jq(feedback_field).prepend("<div class='failed'>" + obj.failed + "</div>");

			recent_cue.innerHTML = obj.failed;
		}
	}

	if (typeof obj === "string")
	{
		jq(feedback_field).prepend(obj);

		recent_cue.innerHTML = obj;
	}

	jq(feedback_field).prepend("<br>");
}

function clear_feedback()
{
	feedback_field.innerHTML = "";
	console.log("clear");
}

/*
* shows a cue and if he has Page property lists cuelist
*/
function show(cue, triggered=true)
{	
	// nur áls recent cue anzeigen wenn es auch ein triggernder cue war
	/*
	if (triggered)
	{
		recent_cue.innerHTML = cue.Name;
	}
	*/
	

	for(v_cue of visible_cues)
	{
		if(v_cue.Name == cue.Name)
		{
			deselect_all();
			v_cue.select();
			break;
		}
	}

	if (cue.Page != undefined)
	{	
		for (var tab_btn of tab_buttons)
		{
			tab_btn.deselect();
			if(tab_btn.b.title == cue.Name)
			{
				tab_btn.select();
			}
		}
		get_cues(cue.Page, sort);
	}
}

function deselect_all()
{
	for(cue of visible_cues)
	{
		cue.deselect();
	}
}

/*
* Arrange cue objects in same order as origin array
*/
function sort(cues, cue_array)
{
	let cue_list = [];
	for (var cue of cue_array)
	{
		for(var cue_item of cues)
		{
			if(cue == cue_item.Name)
			{
				cue_list.push(cue_item);
				break;
			}
		}
	}
	list(cue_list);
}

/*
*  takes a (sorted) list of cues and displays them
*/
function list(cues)
{
	clear();

	for (var cue of cues)
	{
		let cue_bar = new Cue_bar(cue.Name, cue.Comment, cue.Page, cue.Style);
		visible_cues.push(cue_bar);
		ctrl_panel.appendChild(cue_bar.bar);
	}
}

/**
* Build Main cue Tabs
*/
function create(cues)
{
	tab_buttons = [];

	// clear tabs
	while (tabs.firstChild) {
    	tabs.removeChild(tabs.firstChild);
	}

	// clear cue bar
	clear();

	for (var cue of cues)
	{
		let cue_bar = new Cue_bar(cue.Name, cue.Comment, cue.Page, cue.Style);
		if (cue.Page != undefined)
		{
			let tab_btn = new Cue_button(cue.Name, "cue_btn_page, tab_btn", cue_bar, cue.Style);
			tab_buttons.push(tab_btn);
			tabs.appendChild(tab_btn.b);
		}
	}
}


/*
*  remove all items from cuelist
*/
function clear()
{
	visible_cues = [];
	while (ctrl_panel.firstChild) {
    ctrl_panel.removeChild(ctrl_panel.firstChild);
	}
}

/*
*  row with one Button and comment text
*/
function Cue_bar(name, comment, page, style=undefined)
{
	this.Name = name;
	this.selected = false;
	// create bar
	this.bar = document.createElement("div");
	this.bar.className = 'cue_bar';

	this.Page = page;
	if (this.Page == undefined)
	{
		this.button = new Cue_button(this.Name, "cue_btn", this);
	} else {
		this.button = new Cue_button(this.Name, "cue_btn_page", this);
	}

	this.bar.appendChild(this.button.b);

	// add textfield div with comment as text
	this.textfield = document.createElement("div");
	this.textfield.className = 'comment';
	if (comment != undefined)
	{
		this.comment = document.createTextNode(comment);
	} else {
		this.comment = document.createTextNode("-");
	}
	
	this.textfield.appendChild(this.comment);
	this.bar.appendChild(this.textfield);

	if (style != undefined)
	{
		jq(this.button.b).addClass(style);
	}

	this.send_cue = function()
	{
		let cue_object = {"Name": this.Name, "Source":"electron_ctrl"};
		console.log("Dispatch: " + this.Name);
		//feedback(this.Name);
		main.sendUDP(main.config.framework.host, main.config.framework.port, cue_object);

		show(this);
	}

	this.select = function()
	{
		jq(this.bar).addClass('cue_bar_sel');
		jq(this.bar).removeClass('cue_bar');
		this.selected = true;
	}
	this.deselect = function()
	{
		jq(this.bar).addClass('cue_bar');
		jq(this.bar).removeClass('cue_bar_sel');
		this.selected = false;
	}
}
/**
*  Button that triggers a cue or shows a list of cues
*/
function Cue_button(title, type, parent)
{
	// add Button with cuename as Text
	this.b = document.createElement("BUTTON");
	this.type = type;
	this.b.className = type;
	this.b.title = title;
	this.b.label = document.createTextNode(title);
	this.b.appendChild(this.b.label); 

	if (type == "cue_btn" || type == "cue_btn_page")
	{
		this.b.addEventListener('click', function() {parent.send_cue();});
	}
	else
	{
		this.b.addEventListener('click', function() {
			show(parent, false);
		})
	}
	this.select = function()
	{
		this.b.className = type + "_sel";
		// console.log(this.b.className);
	}
	this.deselect = function()
	{
		this.b.className = type;
	}
}